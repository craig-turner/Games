// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
angular.module('gamesIndex', ['ionic'])

.run(function($ionicPlatform, $state) {
  $ionicPlatform.ready(function() {
    

    Parse.initialize('UTz1BF2IsVJVan7MCZdp2jr8TOg4IQpMEMoNCPuV', '0nEq14mIm8QZHRsyAaVQ8ia4QlcJgsKEWCCddCNP');

    var currentUser = Parse.User.current();
    if(currentUser) {
      //Logged in
      $state.go('app.discover');
    } else {
      $state.go('app.splash');
    }


    if (window.cordova) {
      // running on device/emulator
    } else {
      // running in dev mode
      console.log("Application running in browser");
    }

    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if(window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
    }
    if(window.StatusBar) {
      StatusBar.styleDefault();
    }
  });
  
})

.config(function($stateProvider, $urlRouterProvider,$ionicConfigProvider) {

  // Ionic uses AngularUI Router which uses the concept of states
  // Learn more here: https://github.com/angular-ui/ui-router
  // Set up the various states which the app can be in.
  // Each state's controller can be found in controllers.js


  $ionicConfigProvider.tabs.position("top");

  $stateProvider

  .state('app', {
    url: '/app',
    abstract: true,
    templateUrl: "templates/menu.html",
    controller: 'mainController'
  })

  .state('app.splash', {
    url: '/splash',
    views: {
      'menuContent' :{
        templateUrl: "templates/splash.html",
        controller: 'splashController'
      }
    }
  })
  
    // setup an abstract state for the tabs directive
  .state('app.login', {
    url: "/login",
    views: {
      'menuContent' :{
        templateUrl: "templates/login.html",
        controller: 'loginController'
      }
    }
    
  })
  
      // setup an abstract state for the tabs directive
  .state('app.signup', {
    url: "/signup",
    views: {
      'menuContent' :{
        templateUrl: "templates/signup.html",
        controller: 'signupController'
      }
    }
    
  })

  .state('app.discover', {
    url: "/discover",
    views: {
      'menuContent' :{
        templateUrl: "templates/discover.html",
        controller: "discoverController"
      }
    }
  })

  .state('app.game', {
    url: "/game/:id",
    views: {
      'menuContent' :{
        templateUrl: "templates/game.html",
        controller: "gameController"
      }
    }
  })
  
    // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/app/splash');

})


.controller('mainController', function($scope, $state, $ionicHistory){
  $scope.logout = function() {
    Parse.User.logOut();
    $ionicHistory.nextViewOptions({
      disableBack: true
    });
    $state.go('app.splash');
  }
  
  
})

.controller('splashController', function($scope, $state, $ionicHistory, $ionicSideMenuDelegate){

  $ionicSideMenuDelegate.canDragContent(false);
  

})

.controller('signupController', function($scope, $window, $state, $ionicHistory, $ionicSideMenuDelegate,$ionicPopup){

  $ionicSideMenuDelegate.canDragContent(false);
  
  $scope.signupData = {}

  var signupData = $scope.signupData;
  
  $scope.doSignup = function() {
    console.log("Signup Button Pressed.");
    
    var user = new Parse.User();
    user.set("username", signupData.username.toLowerCase());
    user.set("password", signupData.password);
    user.set("email", signupData.email);
    
    // other fields can be set just like with Parse.Object
    //user.set("phone", "415-392-0202");
    
    user.signUp(null, {
      success: function(user) {
        // Hooray! Let them use the app now.
        
        $scope.doSpinner = false;
        
        // Return a success message for signing up
        
        var alertPopup = $ionicPopup.alert({
           title: 'Congratulations!',
           template: 'Successfully signed up!'
         });

        alertPopup.then(function(res) {
           // Do stuff after successful login.
          $ionicHistory.nextViewOptions({
            disableBack: true
          });
          $ionicHistory.goBack();
          //$state.go('app.login');
         });

        
        
      },
      error: function(user, error) {
        // Show the error message somewhere and let the user try again.
        $scope.doSpinner = false;
        // The login failed. Check error to see why.

        var alertPopup = $ionicPopup.alert({
           title: 'Signup Failed',
           template: error.message
         });

        alertPopup.then(function(res) {
           console.log(error.message);
         });
      }
    });
    
    
    
  }

})

.controller('gameController', function($scope, $state, $ionicHistory, $ionicSideMenuDelegate, $stateParams, $ionicPopup){

  console.log($stateParams.id);

  $scope.gameInfo = {};

  var user = Parse.User.current();
  var relation = user.relation("watched_games");


  // Fetch specific game

  var GameScore = Parse.Object.extend("games");
  var query = new Parse.Query(GameScore);
  query.get($stateParams.id, {
    success: function(gameScore) {
      // The object was retrieved successfully.

      console.log(gameScore);
      $scope.gameInfo = gameScore;

    },
    error: function(object, error) {
      // The object was not retrieved successfully.
      // error is a Parse.Error with an error code and message.
      console.log("Error: " + error.code + " " + error.message);

    }
  });


  $scope.doSpinner = false;


  var query = relation.query();
  query.equalTo("watched_games", user);
  query.include("watched_games");
  query.find({
    success:function(list) {
      // list contains post liked by the current user which have the title "I'm Hungry".
      console.log("GAME!");
      console.log(list);
    }
  });



  $scope.likeGame = function() {
    
    relation.add($scope.gameInfo);
    user.save(null, {
      success: function(data) {
        console.log("Saved!");
        $scope.doSpinner = false;
        // The login failed. Check error to see why.

        var alertPopup = $ionicPopup.alert({
           title: 'Successfully Added',
           template: 'Added to your watch list.'
         });

        alertPopup.then(function(res) {
           console.log(error.message);
         });
      },
      error: function(error)
      {
        console.log("Error: " + error.code + " " + error.message);
      }
    });
  }




})

.controller('loginController', function($scope, $state, $ionicHistory, $ionicSideMenuDelegate, $ionicPopup){
  $scope.doSpinner = false;


  $ionicSideMenuDelegate.canDragContent(false);

  $scope.loginData = {}

  var user = $scope.loginData;

  $scope.doLogin = function() {

    $scope.doSpinner = true;


    Parse.User.logIn(user.username.toLowerCase(), user.password, {
      success: function(user) {

        $scope.doSpinner = false;

        // Do stuff after successful login.
        $ionicHistory.nextViewOptions({
          disableBack: true
        });
        $state.go('app.discover');
        
      },
      error: function(user, error) {

        $scope.doSpinner = false;
        // The login failed. Check error to see why.

        var alertPopup = $ionicPopup.alert({
           title: 'Login Failed',
           template: error.message
         });

        alertPopup.then(function(res) {
           console.log(error.message);
         });


      }
    });
  }
})


.controller('discoverController', function($scope, $ionicScrollDelegate) {

  // Initialize

  $scope.games = [];
  $scope.released = [];
  $scope.popular = [];
  $scope.showSearch = false;

  // Fetch all games
  var GameScore = Parse.Object.extend("games");
  var query = new Parse.Query(GameScore);
  query.lessThan("release_uk", new Date());
  query.find({
    success: function(results) {
      console.log("Successfully retrieved " + results.length + " scores.");
      // Do something with the returned Parse.Object values
      for (var i = 0; i < results.length; i++) { 
        var object = results[i];
        $scope.games.push(object);
        console.log(object);
      }
    },
    error: function(error) {
      console.log("Error: " + error.code + " " + error.message);
    }
  });

  // Fetch all games
  var GameScore1 = Parse.Object.extend("games");
  var query1 = new Parse.Query(GameScore1);
  query1.greaterThan("release_uk", new Date());
  query1.find({
    success: function(results) {
      console.log("Successfully retrieved " + results.length + " scores.");
      // Do something with the returned Parse.Object values
      for (var i = 0; i < results.length; i++) { 
        var object = results[i];
        $scope.released.push(object);
        console.log(object);
      }
    },
    error: function(error) {
      console.log("Error: " + error.code + " " + error.message);
    }
  });

  // Fetch all games
  var GameScore2 = Parse.Object.extend("games");
  var query2 = new Parse.Query(GameScore2);
  query2.greaterThan("release_uk", new Date());
  query2.find({
    success: function(results) {
      console.log("Successfully retrieved " + results.length + " scores.");
      // Do something with the returned Parse.Object values
      for (var i = 0; i < results.length; i++) { 
        var object = results[i];
        $scope.popular.push(object);
        console.log(object);
      }
    },
    error: function(error) {
      console.log("Error: " + error.code + " " + error.message);
    }
  });


  $scope.doSearch = function() {
    //$ionicScrollDelegate.scrollTo(0, 44, true);

    if($scope.showSearch)
    {
      $scope.showSearch = false;
    } else {
      $scope.showSearch = true;
    }
  }

  $scope.clearSearch = function() {
    //$ionicScrollDelegate.scrollTo(0, 44, true);
    $scope.games.searchQuery = '';
  }




})

.directive('backImg', function(){
  return function(scope, element, attrs){
    var url = attrs.backImg;
    var content = element.find('a');
    content.css({
      'background': 'url('+ url + ')',
      'background-size' : 'cover'
    });
  };
});
